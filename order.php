<?php
    require_once 'lib/router.php';
    require_once 'lib/database.php';

    function add($id, $book, $qty) {
        $db = new Database();
        echo json_encode(['success' => $db->add_book_to_order($id, $book, $qty)]);
    }

    function validate($id) {
        $db = new Database();
        $db->validate_order($id);
        redirect('index.php');
    }

    function cancel($id) {
        $db = new Database();
        $db->delete_order($id);
        redirect('index.php');
    }

    function new_order() {
        $db = new Database();
        if (isset($_COOKIE['USER'])) {
            if($db->create_order($_COOKIE['USER'])) {
                redirect('index.php');
            } else {
                redirect('index.php?error=unknown');
            }
        }
    }

    function delete_book($cmd, $book) {
        $db = new Database();

        if($db->delete_book_from_order($cmd, $book)) {
            redirect('index.php');
        } else {
            redirect('index.php?error=unknown');
        }
    }


    if (isset($_GET['action']) && isset($_GET['id'])) {
        $action = $_GET['action'];
        switch ($action) {
            case 'validate':
                validate($_GET['id']);
                break;
            case 'cancel':
                cancel($_GET['id']);
                break;
            case 'add':
                add($_GET['id'], $_GET['book'], $_GET['qty']);
                break;
            case 'new':
                new_order();
                break;
            case 'delete_book':
                delete_book($_GET['id'], $_GET['book']);
                break;
            default:
                echo '404';
                break;
        }
    }
?>
