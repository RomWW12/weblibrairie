<?php


    require 'vendor/autoload.php';
    require 'lib/router.php';
    require 'lib/database.php';

    $errors = [
        'db_error' => 'La base de donnée n\'es pas disponible. Veuillez réessayer.',
        'invalid_user' => 'Nom d\'utilisateur ou mot de passe incorrect',
        'error_book_insert' => 'Erreur lors de la création de l\'ouvrage.'
    ];

    $db = new Database();

    $data = array();

    if (isset($_GET['error'])) {
        $data['error'] = $errors[$_GET['error']];
    }

    if (isset($_COOKIE['USER'])) {
        $data['user'] = $db->get_user($_COOKIE['USER']);
        if (isset($data['user']['libraire']) && $data['user']['libraire'] == 1) {
            $data['books'] = array_reverse($db->select('ouvrages'));

            $data['clients'] = array_reverse(array_filter($db->select('personnes'),
                function($client) {
                    return $client['libraire'] == 0;
                }
            ));

            $data['commandes'] = array_reverse(
                $db->select('commandes JOIN personnes ON commandes.idpersonne = personnes.idpersonne ORDER BY validee;')
            );

            renderResponse('index_libraire.html', $data);
        } else {
            $data['commandes'] = array_reverse(
                array_map(function($cmd) use ($db) {
                    $cmd['total_qty'] = $db->count_qty($cmd['idcmd'])[0];
                    return $cmd;
                } ,$db->select('commandes JOIN personnes ON commandes.idpersonne = personnes.idpersonne WHERE commandes.idpersonne='. $data['user']['idpersonne'] .' ORDER BY validee;'))
            );

            renderResponse('index_client.html', $data);
        }

    } else {
        renderResponse('index.html', $data);
    }


?>
