$(() => {
    function enable_events() {
        $("#titreselect").change(() => {
            var id = $('#titreselect option:selected').val();

            $('#auteurselect option[value="' + id + '"]').prop('selected', true);
        });

        $("#auteurselect").change(() => {
            var id = $('#auteurselect option:selected').val();

            $('#titreselect option[value="' + id + '"]').prop('selected', true);
        });

        $("#submitAddBook").click(() => {
            var cmd = $("#commandID").val();
            var book_id = $('#titreselect option:selected').val();
            var qty = $('#quantity').val();

            $.ajax({
                url: 'http://'+ window.location.host +'/order.php?action=add&id=' + cmd + '&book=' + book_id + '&qty=' + qty,
                success: (result) => {
                    if (JSON.parse(result).success) {
                        $(location).attr('href', 'http://'+ window.location.host +'/index.php');
                    } else {
                        console.log('error');
                    }

                }
            });
        });
    }

    $("a[id^='a']").on('click', (e) => {
        var id =  e.target.attributes['aria-controls'].value;

        $.ajax({
            url: 'get_details.php?id=' + id + '&client=true',
            success: (result) => {
                $("#" + id).find('.card').html(result);
                $("#" + id).collapse('toggle');

                enable_events();
            }
        });
    });
});
