<?php
    require_once 'lib/database.php';
    require_once 'lib/router.php';

    if (isset($_GET['id'])) {
        $db = new Database();

        $bouquins = isset($_GET['client']) ? $db->select('ouvrages') : [];

        renderResponse('details.html', [
            'allbooks' => $bouquins,
            'cmd' => $_GET['id'],
            'books' => $db->select('lignescmd JOIN ouvrages ON ouvrages.idouvrage = lignescmd.idouvrage
                        JOIN commandes ON lignescmd.idcmd = commandes.idcmd WHERE lignescmd.idcmd=' . $_GET['id']),
            'client' => isset($_GET['client'])
        ]);
    } else {
        return null;
    }
?>
