# Mineure Web - PHP

## Requirements

 - `php7.0-xml`: pour l'installer, exécuter: `sudo apt-get install php7.0-xml`
 - Twig: il faut avoir au préalable installé __Composer__. Ensuite, il suffit de lancer:

```bash
	cd path/to/project
	composer install
```

 - Cette application utilise également JQuery et Bootstrap, il n'a rien à faire pour les installer, simplement s'assurer que le serveur ait accès à internet.

## Mise en route

Normalement, l'application est faite pour tourner `/var/www/html` avec la config d'apache par défaut, mais j'ai essayé d'être le plus _directory agnostic_ possible.

La base de donnée, fonctionne par défaut avec les credentials renseignés dans le sujet du BE. Néanmoins ils peuvent être modifiés dans `lib/constants.php`
