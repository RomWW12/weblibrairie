<?php
    require_once 'lib/router.php';

    $errors = [
        'db_error' => 'La base de donnée n\'es pas disponible. Veuillez réessayer.',
        'invalid_user' => 'Nom d\'utilisateur ou mot de passe incorrect',
        'error_book_insert' => 'Erreur lors de la création de l\'ouvrage.'
    ];

    $data = array();

    if (isset($_GET['error'])) {
        $data['error'] = $errors[$_GET['error']];
    }

    renderResponse('login.html', $data);
?>
