<?php
    require 'lib/router.php';
    require 'lib/database.php';
    $db = new Database();

    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        renderResponse('register.html');
    } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if ($db->insert_user($_POST)) {
            setcookie('USER', $db->authenticate($_POST['fname'], $_POST['password'])['idpersonne'], time()+3600);
            redirect('index.php');
        }
    }

?>