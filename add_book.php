<?php
    require_once 'lib/database.php';
    require_once 'lib/router.php';

    $db = new Database();

    $redirect = 'index.php';
    if(!$db->insert_book($_POST)) {
        $redirect .= '?error=error_book_insert';
    }

    redirect($redirect);
?>