<?php

require_once 'lib/constants.php';

class Database {

    private $mysqli;

    function __construct() {

        $mysqli = new mysqli(
            Constants::MYSQL_HOST,
            Constants::MYSQL_USERNAME,
            Constants::MYSQL_PASSWORD,
            Constants::MYSQL_DATABASE
        );
        $this->mysqli = $mysqli;
        $this->mysqli->set_charset('utf-8');

        if ($mysqli->connect_errno) {
            echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
    }

    function insert_book($book) {
        $query = 'INSERT INTO ouvrages (titre, auteur, prix) VALUES (?, ?, ?)';

        $stmt = $this->mysqli->prepare($query);

        $stmt->bind_param('ssd', utf8_decode($book['titre']), utf8_decode($book['auteur']), $book['prix']);

        return $stmt->execute();

    }

    function insert_user($user) {
        var_dump($user);
        $query = 'INSERT INTO personnes (nom, prenom, adresse, password, libraire) VALUES (?, ?, ?, ?, 0)';

        $stmt = $this->mysqli->prepare($query);

        $stmt->bind_param('ssss', utf8_decode($user['lname']), utf8_decode($user['fname']),
            utf8_decode($user['address']), utf8_decode($user['password']));

        return $stmt->execute();

    }

    function select($table, $attributes = null) {
        if (is_null($attributes)) {
            $query = 'SELECT * FROM ' . $table;
        } else {
            $query = 'SELECT ' . join(',', $attributes) . ' FROM ' . $table;
        }

        $result_query = $this->mysqli->query($query);

        $result = [];
        if ($result_query) {
            for ($row_no = $result_query->num_rows - 1; $row_no >= 0; $row_no--) {
                $result_query->data_seek($row_no);
                $row = $result_query->fetch_assoc();
                foreach ($row as $key => $element) {
                    $row[$key] = utf8_encode($element);
                }
                array_push($result, $row);
            }
        } else return [];

        return $result;
    }

    function get_user($id) {
        $query = 'SELECT * FROM personnes WHERE idpersonne = ?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('i', $id);

            if($stmt->execute()) {
                return $stmt->get_result()->fetch_array();
            } else {
                return $this->mysqli->error;
            }
        } else {
            return $this->mysqli->error;
        }
    }

    function authenticate($username, $password) {
        $query = 'SELECT * FROM personnes WHERE nom = ? AND password = ?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('ss', $username, $password);

            if($stmt->execute()) {
                return $stmt->get_result()->fetch_array();
            } else {
                return $this->mysqli->error;
            }
        } else {
            return $this->mysqli->error;
        }
    }

    function create_order($user) {
        $query = 'INSERT INTO commandes (idpersonne, date, validee) VALUES (?, ?, 0)';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('is', $user, date('Y-m-d'));

            return $stmt->execute();
        }
    }

    function validate_order($id) {
        $query = 'UPDATE commandes SET validee = 1 WHERE idcmd = ?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('i', $id);

            return $stmt->execute();
        }
    }

    function delete_order($id) {
        $query = 'DELETE FROM lignescmd WHERE idcmd = ?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('i', $id);

            $stmt->execute();
        }

        $query = 'DELETE FROM commandes WHERE idcmd = ?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('i', $id);

            return $stmt->execute();
        }
    }

    function add_book_to_order($id, $book, $qty) {
        $query = 'INSERT INTO lignescmd (idcmd, idouvrage, qte) VALUES (?, ?, ?)';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('iii', $id, $book, $qty);

            return $stmt->execute();
        }
    }

    function delete_book_from_order($cmd, $book) {
        $query = 'DELETE FROM lignescmd WHERE idcmd=? AND idouvrage=?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('ii', $cmd, $book);

            return $stmt->execute();
        }
    }

    function count_qty($cmd) {
        $query = 'SELECT COUNT(qte) from lignescmd WHERE idcmd=?';

        if($stmt = $this->mysqli->prepare($query)) {

            $stmt->bind_param('i', $cmd);

            if($stmt->execute()) {
                return $stmt->get_result()->fetch_array();
            }
        }
    }
}
?>
