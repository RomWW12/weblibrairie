$(() => {
    $("a[id^='a']").on('click', (e) => {
        var id =  e.target.attributes['aria-controls'].value;

        $.ajax({
            url: 'get_details.php?id=' + id,
            success: (result) => {
                $("#" + id).find('.card').html(result);
                $("#" + id).collapse('toggle');
            }
        });
    });
});