$(() => {
    $('#cpassword').focusout(() => {
        var c = $('#cpassword');
        var p = $('#password');
        if (c.val() !=  p.val()) {
            c.addClass('is-invalid');
            p.addClass('is-invalid');
        }
    });
    $("form").submit((e) => {
        var c = $('#cpassword');
        var p = $('#password');
        if (c.val() != p.val()) {
            e.preventDefault();
        }
    });
});
