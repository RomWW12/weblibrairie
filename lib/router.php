<?php
    require_once 'vendor/autoload.php';
    require_once 'lib/constants.php';

   function renderResponse($template, $data = []) {
       $loader = new Twig_Loader_Filesystem('views');
       $twig = new Twig_Environment($loader);
       $template = $twig->load($template);
       echo $template->render($data);
   }

   function renderJSON($data) {
       header('Content-Type: application/json');
       echo json_encode(['data' => $data]);
   }

   function redirect($url) {
       header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $url);
       exit();
   }
 ?>
