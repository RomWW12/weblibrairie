<?php
    require_once 'lib/database.php';
    require_once 'lib/router.php';

    $db = new Database();

    $result = $db->authenticate($_POST['username'], $_POST['password']);

    if (is_string($result)) {

        redirect('login.php?error=db_error');
    } else if (!is_null($result)) {
        setcookie('USER', $result['idpersonne'], time()+3600);

        redirect('index.php');
    } else {
        redirect('login.php?error=invalid_user');
    }
?>
